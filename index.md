---
layout: page
---
{% for section in site.data.homepage.sections %}
<div class="main-item">
  <h2>{{section.name}}</h2>
  <div class="site-list">
    {% for a_site in section.sites %}
    <div class="main-line-item">
      <a href="{{ a_site.url }}">
        <div class="site-icon">
          <img src="{{ a_site.icon_url }}" alt="{{ a_site.text }} icon">
        </div>
        <div class="site-text">{{ a_site.text }}</div>
      </a>
    </div>
    {% endfor %}
  </div>
</div>
{% endfor %}
